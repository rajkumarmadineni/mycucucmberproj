package javaActivity1;

public class Car {
	String colour;
	String transmission;
	int make, tyres, doors;
	
	Car() {
		tyres = 4;
		doors = 4;	
	}
	
	void displayCharacteristics() {
		System.out.println("Colour is : "+colour);
		System.out.println("Transmission is : "+transmission);
		System.out.println("Make is : "+make);
		System.out.println("Tyres are : "+tyres);
		System.out.println("Doors are : "+doors);
	}
	
	void forward() {
		System.out.println("Car is moving forward...");
	}
	
	void back() {
		System.out.println("Car has stopped...");
	}
}
