package seleniumProgs;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HeadlessDriver {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		FirefoxOptions options = new FirefoxOptions();
		options.setHeadless(true);
		WebDriver driver = new FirefoxDriver(options);
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		driver.get("http://www.google.com");
		
		wait.until(ExpectedConditions.titleContains("Google"));
		
		driver.findElement(By.name("q")).sendKeys("Cheese",Keys.RETURN);
		
		wait.until(ExpectedConditions.titleContains("Cheese"));
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		Thread.sleep(5000);
		
		String filename = "Screenshot_"+System.currentTimeMillis();
		
        File screenshot = ((RemoteWebDriver) driver).getScreenshotAs(OutputType.FILE);
    	
        File outputFile = new File("screenshots/" + filename + ".png");
		
        FileUtils.copyFile(screenshot, outputFile);
	}

}
