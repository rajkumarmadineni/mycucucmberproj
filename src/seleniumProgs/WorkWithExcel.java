package seleniumProgs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WorkWithExcel {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String path = "D:\\Eclipse\\Eclipse Programs\\credentials.xlsx";
		String sheetName = "Data";
		
		//fileRead(path, sheetName);
		
        String[] dataToWrite = {"admin2", "password2"};
        writeExcel(path, "NoData", dataToWrite);
	}
	
    public static void writeExcel(String filePath, String sheetName, String[] dataToWrite) throws IOException {
        // Create an object of File class to open the XLSX file
        File file = new File(filePath);
	
        // Create an object of FileInputStream class to read excel file
        FileInputStream inputStream = new FileInputStream(file); 
	
        // Create a Workbook
        Workbook book = null;
	
        // Find the file extension by splitting file name in substring and getting only extension name
        String fileExtensionName = filePath.substring(filePath.indexOf("."));
	
        // Check if it is an xlsx file
        if(fileExtensionName.equals(".xlsx")) {
            // If it's an xlsx file, create object of XSSFWorkbook class
            book = new XSSFWorkbook(inputStream);
        }
        // Else if it is an xls file
        else if(fileExtensionName.equals(".xls")) {
	
            // then create object of HSSFWorkbook class
            book = new HSSFWorkbook(inputStream);
        }
	
        // Read the sheet inside by it's name
        Sheet sheet = book.getSheet(sheetName);
	
        // Find the number of rows in the file
        int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();

        // Get the first row from the sheet
        Row row = sheet.getRow(0);
	
        // Create a new row and append it to the end of the sheet
        Row newRow = sheet.createRow(rowCount+1);
	
        //Create a loop over the cell of newly created Row
        if (rowCount != 0) {
	        for(int j = 0; j < row.getLastCellNum(); j++){
	            //Fill data in row
	            Cell cell = newRow.createCell(j);
	            cell.setCellValue(dataToWrite[j]);
	        }
        }
        else {
        	for(int j = 0; j < dataToWrite.length; j++){
	            //Fill data in row
        		Cell cell = newRow.createCell(j);
        		cell.setCellValue(dataToWrite[j]);
        	}
        }
        //Close input stream
        inputStream.close();
	
        //Create an object of FileOutputStream class to create write data in excel file
        FileOutputStream outputStream = new FileOutputStream(file);
	
        //write data in the excel file	
        book.write(outputStream);

        //close output stream
        outputStream.close();
    }
    
	public static void fileRead(String filePath, String sheetName) throws IOException {
		File file = new File(filePath);
		FileInputStream inputStream = new FileInputStream(file);
		
		Workbook book = null;
		String extension = filePath.substring(filePath.indexOf("."));
		
        // Check if it is an xlsx file
		
        if(extension.equals(".xlsx")) {
            // If it's an xlsx file, create object of XSSFWorkbook class
            book = new XSSFWorkbook(inputStream);
        }
        // Else if it is an xls file
        else if(extension.equals(".xls")) {
            // then create object of HSSFWorkbook class	
            book = new HSSFWorkbook(inputStream);
        }

        // Read the sheet inside by it's name
        Sheet sheet = book.getSheet(sheetName);
	
        // Find the number of rows in the file
        int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();

        // Create a loop over all the rows to read it
        for (int i = 0; i < rowCount+1; i++) {
            Row row = sheet.getRow(i);
            
            //Create a loop to print cell values in a row
            for (int j = 0; j < row.getLastCellNum(); j++) {
                //Print Excel Data into the Console
                System.out.print(row.getCell(j).getStringCellValue() + " || ");
            }
            System.out.println();	
        }
	}

}
