package seleniumProgs;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class SampleDemo {

	static WebDriver driver = null;
	
	public static WebDriver createDriver(String driverType) {
		switch (driverType) {
		case "firefox":
						driver = new FirefoxDriver();
						break;
		case "ie":
						driver = new InternetExplorerDriver();
						break;
		case "edge":	
						driver = new EdgeDriver();
						break;
		case "chrome":
						driver = new ChromeDriver();
						break;
		default:
						System.out.println("Invalid driver entered...");
						break;
		}
		return driver;
	}
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		//submitForm();
		//sample();
		//activity4_3();
		//disableDemo();
		//isSelectedDemo();
		//isEnabledDemo();
		//findElementsDemo();
		//dropdownDemo();
		//multiSelectDropdownDemo();
		//dynamicDemo();
		//ajaxContent();
		//dynamicAttributes();
		//dragandDrop();
		//alertDemo();
		//windowHandlesDemo();
		//framesDemo();
		nestedFramesDemo();
		
	}
	static public void submitForm() throws InterruptedException {
		driver.get("https://www.training-support.net\\selenium\\simple-form");
		Thread.sleep(10);
		WebElement fname = driver.findElement(By.id("firstName"));
		fname.sendKeys("Rajkumar");
		
		WebElement lname = driver.findElement(By.id("lastName"));
		lname.sendKeys("Madineni");
		
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys("raj@gmail.com");
		
		WebElement mobile = driver.findElement(By.id("number"));
		mobile.sendKeys("1234567890");
		
		WebElement message = driver.findElement(By.tagName("textarea"));
		message.sendKeys("This is from Selenium with Java class");
		
		//WebElement sub = driver.findElement(By.cssSelector("input.ui.green.button"));
		//sub.click();
		WebElement sub = driver.findElement(By.id("simpleForm"));
		sub.submit();
	}
	static public void sample() throws InterruptedException {
		driver.get("https://www.training-support.net");
		Thread.sleep(10);
		Assert.assertTrue("Training Support".equals(driver.getTitle()));
		/* driver.wait(10); */
		//WebElement ele = driver.findElement(By.linkText("About Us"));
		//WebElement ele = driver.findElement(By.xpath("/html/body/div/div/div/a"));
		WebElement ele = driver.findElement(By.xpath("//a[text()='About Us']"));
		ele.click();
		Thread.sleep(10);
		Assert.assertTrue("About Training Support".equals(driver.getTitle()));
		driver.close();
	}
	
	static public void xpathDemo() {
		driver = createDriver("chrome");
		driver.get("https://www.training-support.net\\selenium\\simple-form");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Rajkumar");
		
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Madineni");
		
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("raj@gmail.com");
		
		driver.findElement(By.xpath("//input[@id='number']")).sendKeys("1234567890");
		
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div/div[5]/textarea")).sendKeys("This is from Selenium with Java class");
		
		//WebElement sub = driver.findElement(By.cssSelector("input.ui.green.button"));
		//sub.click();
		WebElement sub = driver.findElement(By.id("simpleForm"));
		sub.submit();
	}
	
	static public void activity4_3() {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\target-practice");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebElement ele1 = driver.findElement(By.xpath("//*[@id='third-header']"));
		System.out.println("Third element is : "+ele1.getTagName());
		
		WebElement ele2 = driver.findElement(By.xpath("//*[text()='Fifth header']"));
		System.out.println("Fifth element is : "+ele2.getCssValue("color"));
		
		WebElement ele3 = driver.findElement(By.xpath("//button[text()='Violet']"));
		System.out.println("Voilet button class attibute values are : "+ele3.getAttribute("class"));
		
		WebElement ele4 = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div/div/div/div[2]/div[3]/button[2]"));
		System.out.println("Grey button class attibute values are  : "+ele4.getAttribute("class"));
		
	}
	
	static public void disableDemo() {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\dynamic-controls");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebElement ele1 = driver.findElement(By.className("willDisappear"));
		System.out.println("Is checkbox visible on the page :"+ele1.isDisplayed());
		
		driver.findElement(By.id("toggleCheckbox")).click();
		System.out.println("Is checkbox visible on the page :"+ele1.isDisplayed());
	}
	
	static public void isSelectedDemo() {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\dynamic-controls");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebElement ele1 = driver.findElement(By.xpath("//input[@name='toggled']"));
		System.out.println("Is checkbox visible on the page :"+ele1.isDisplayed());
		System.out.println("Is checkbox checked :"+ele1.isSelected());
		
		ele1.click();
		System.out.println("Is checkbox checked :"+ele1.isSelected());
	
		driver.close();
	}
	
	static public void isEnabledDemo() {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\dynamic-controls");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebElement ele1 = driver.findElement(By.xpath("//input[@id='input-text']"));
		System.out.println("******Before cliked on Enable Input button*******");
		System.out.println("Is textbox visible on the page :"+ele1.isDisplayed());
		System.out.println("Is textbox checked :"+ele1.isSelected());
		System.out.println("Is textbox checked :"+ele1.isEnabled());
		
		driver.findElement(By.xpath("//button[@id='toggleInput']")).click();
		
		System.out.println("******After cliked on Enable Input button*******");
		System.out.println("Is textbox visible on the page :"+ele1.isDisplayed());
		ele1.click();
		System.out.println("Is textbox checked :"+ele1.isSelected());
		System.out.println("Is textbox checked :"+ele1.isEnabled());
		
		driver.close();
	}
	
	static public void findElementsDemo( ) {
		
		driver = createDriver("firefox");
		driver.get("https://training-support.net\\selenium\\tables");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		//First Table
		List <WebElement> cols = driver.findElements(By.xpath("//table[contains(@class, 'striped ')]/tbody/tr[1]/td"));
		System.out.println("No. of Columns are : "+cols.size());
		
		List <WebElement> rows = driver.findElements(By.xpath("//table[contains(@class, 'striped ')]/tbody/tr"));
		System.out.println("No. of Rows are : "+rows.size());
	
		List <WebElement> thirdrow = driver.findElements(By.xpath("//table[contains(@class, 'striped ')]/tbody/tr[3]/td"));
		System.out.println(" third row of the table values are : ");
		for (WebElement webElement : thirdrow) {
			System.out.println(webElement.getText());
		}
		
		WebElement value  = driver.findElement(By.xpath("//table[contains(@class, 'striped ')]/tbody/tr[2]/td[2]"));
		System.out.println(" second row and second column value is : "+ value.getText());
		
		//Second Table
		List <WebElement> cols1 = driver.findElements(By.xpath("//table[contains(@class,'sortable')]/tbody/tr[1]/td"));
		System.out.println("No. of Columns are : "+cols1.size());
				
		List <WebElement> rows1 = driver.findElements(By.xpath("//table[contains(@class,'sortable')]/tbody/tr"));
		System.out.println("No. of Rows are : "+rows1.size());
				
		WebElement value1  = driver.findElement(By.xpath("//table[contains(@class,'sortable')]/tbody/tr[2]/td[2]"));
		System.out.println(" Before Sorting second row and second column value is : "+ value1.getText());
				
		driver.findElement(By.xpath("//table[contains(@class,'sortable')]/thead/tr/th[1]")).click();
		
		WebElement value2  = driver.findElement(By.xpath("//table[contains(@class,'sortable')]/tbody/tr[2]/td[2]"));
		System.out.println("After Sorting second row and second column value is : "+ value2.getText());
	}
	
	static public void dropdownDemo() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://training-support.net\\selenium\\selects");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement sel  = driver.findElement(By.id("single-select"));
		Select s = new Select(sel);
		s.selectByVisibleText("Option 2");
		Thread.sleep(3);
		s.selectByIndex(3);
		Thread.sleep(3);
		s.selectByValue("4");
		List <WebElement> options = s.getOptions();
		System.out.println("Options from select box are : ");
		for (WebElement webElement : options) {
			System.out.println(webElement.getText());
		}
	}
	
	static public void multiSelectDropdownDemo() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://training-support.net\\selenium\\selects");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement sel  = driver.findElement(By.id("multi-select"));
		Select s = new Select(sel);
		System.out.println("Is this multiple SelectBox : "+s.isMultiple());
		
		s.selectByVisibleText("Javascript");
		Thread.sleep(4);
		s.selectByValue("node");
		Thread.sleep(4);
		s.selectByIndex(3);
		Thread.sleep(4);
		s.selectByIndex(4);
		Thread.sleep(4);
		s.selectByIndex(5);
		Thread.sleep(4);
		s.deselectByValue("node");
		Thread.sleep(4);
		s.deselectByIndex(6);
		Thread.sleep(4);
		System.out.println("First Selected option is : "+s.getFirstSelectedOption().getText());
		
		List <WebElement> list = s.getAllSelectedOptions();
		System.out.println("Slelected Options are : ");
		for (WebElement webElement : list) {
			System.out.println(webElement.getText());
		}
	}
	
	static public void dynamicDemo() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\dynamic-controls");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebElement ele1 = driver.findElement(By.xpath("//input[@name='toggled']"));
		System.out.println("Is checkbox visible on the page :"+ele1.isDisplayed());
		
		WebElement ele2 = driver.findElement(By.id("toggleCheckbox"));
		ele2.click();
		System.out.println("Is checkbox visible on the page (after clicked on button) :"+ele1.isDisplayed());
		Thread.sleep(5);
		
		ele2.click();
		System.out.println("Is checkbox visible on the page(after clicking again on button) :"+ele1.isDisplayed());
		Thread.sleep(5);
		
		//driver.close();
	}
	
	static public void ajaxContent() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\ajax");
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebElement ele1 = driver.findElement(By.xpath("//button[text()='Change Content']"));
		ele1.click();
		
		WebDriverWait wait = new WebDriverWait(driver,20);
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h1"), "HELLO!"));
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.tagName("h3"), "I'm late!"));
		
		WebElement ele2 = driver.findElement(By.xpath("//div[@id='ajax-content']/h1"));
		System.out.println("After waiting Text is : "+ele2.getText());
		
		WebElement ele3 = driver.findElement(By.xpath("//div[@id='ajax-content']/h3"));
		System.out.println("After waiting Text is : "+ele3.getText());
		
		//driver.close();
	}
	
	static public void dynamicAttributes() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\dynamic-attributes");
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		driver.findElement(By.xpath("//input[starts-with(@class,'username-')]")).sendKeys("admin");
		Thread.sleep(10);
		driver.findElement(By.xpath("//input[starts-with(@class,'password-')]")).sendKeys("password");
		Thread.sleep(10);
		driver.findElement(By.xpath("//button[@class='ui button']")).click();
		
		WebDriverWait wait = new WebDriverWait(driver,20);
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@id='action-confirmation']"), "Welcome Back, admin"));
		
		WebElement ele2 = driver.findElement(By.xpath("//div[@id='action-confirmation']"));
		System.out.println("After waiting Text is : "+ele2.getText());
		
		//driver.close();
	}
	
	static public void dragandDrop() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\drag-drop");
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		
		WebDriverWait wait = new WebDriverWait(driver,20);
		
		Actions action = new Actions(driver);
		
		WebElement ball = driver.findElement(By.id("draggable"));
		
		WebElement zone1 = driver.findElement(By.id("droppable"));
		
		WebElement zone2 = driver.findElement(By.id("dropzone2"));
		
		action.dragAndDrop(ball, zone1).build().perform();
		wait.until(ExpectedConditions.attributeToBeNotEmpty(zone1, "background-color"));
		System.out.println("Dropped into zone1");
		
		Thread.sleep(10);
		
		action.dragAndDrop(ball, zone2).build().perform();
		wait.until(ExpectedConditions.attributeToBeNotEmpty(zone2, "background-color"));
		System.out.println("Dropped into zone2");
		
	}
	
	static public void alertDemo() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\javascript-alerts");
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		Thread.sleep(5000);
		
	
		//Simple Alert
		driver.findElement(By.cssSelector("button#simple")).click();
		Thread.sleep(3000);
		Alert alert = driver.switchTo().alert();
		System.out.println("Alert text is : "+alert.getText());
		alert.accept();
		
		//Confirm Alert
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("button#confirm")).click();
		System.out.println("Alert text is : "+alert.getText());
		Thread.sleep(3000);
		alert.accept();
		//alert.dismiss();
		
		//Prompt Alert
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("button#prompt")).click();
		System.out.println("Alert text is : "+alert.getText());
		alert.sendKeys("Sending value");
		Thread.sleep(3000);
		alert.dismiss();
	}
	
	static public void windowHandlesDemo() throws InterruptedException {
		driver = createDriver("firefox");
		driver.get("https://www.training-support.net\\selenium\\tab-opener");
		
		Thread.sleep(5000);
		String title = driver.getTitle();
		System.out.println("Title of the Main page is : "+title);
		Thread.sleep(5000);
		
		driver.findElement(By.id("launcher")).click();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		
		String mainwindow = driver.getWindowHandle();
		System.out.println("Main window Title is : "+mainwindow);
		
		Set<String> windows = driver.getWindowHandles();
		for (String string : windows) {
            driver.switchTo().window(string);
		}
        //Print the handle of the current window
        System.out.println(driver.getWindowHandle());
	
        //Wait for page to load completely
        wait.until(ExpectedConditions.titleIs("Newtab"));
	
        //Print New Tab Title
        System.out.println("New Tab Title is: " + driver.getTitle());
	
        //Get heading on new page
        //Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.content")));
        String newTabText = driver.findElement(By.cssSelector("div.content")).getText();
        System.out.println("New tab heading is: " + newTabText);
	
        //Close the browser
        driver.quit();
		}
	
		static public void framesDemo() throws InterruptedException {
			driver = createDriver("firefox");
			driver.get("https://www.training-support.net\\selenium\\iframes");
			
			Thread.sleep(5000);
			String title = driver.getTitle();
			System.out.println("Title of the Main page is : "+title);
			Thread.sleep(5000);
			
			driver.switchTo().frame(0);
			
			WebElement ele1 = driver.findElement(By.cssSelector("div.content"));
			System.out.println("Frame 1 is : "+ele1.getText());
			
			WebElement fbutton1 = driver.findElement(By.id("actionButton"));
			System.out.println("Button Text before click is :"+fbutton1.getText());
			System.out.println("Button color before click is : "+fbutton1.getCssValue("background-color"));
			
			fbutton1.click();
			
			System.out.println("Button Text after click is :"+fbutton1.getText());
			System.out.println("Button color after click is : "+fbutton1.getCssValue("background-color"));
			
			driver.switchTo().defaultContent();
			driver.switchTo().frame(1);
			
			WebElement ele2 = driver.findElement(By.cssSelector("div.content"));
			System.out.println("Frame 2 is : "+ele2.getText());
			
			WebElement fbutton2 = driver.findElement(By.id("actionButton"));
			System.out.println("Button Text before click is :"+fbutton2.getText());
			System.out.println("Button color before click is : "+fbutton2.getCssValue("background-color"));
			
			fbutton2.click();
			
			System.out.println("Button Text after click is :"+fbutton2.getText());
			System.out.println("Button color after click is : "+fbutton2.getCssValue("background-color"));
			
			
		}
		
		public static void nestedFramesDemo() throws InterruptedException {
			driver = createDriver("firefox");
			driver.get("https://www.training-support.net\\selenium\\nested-iframes");
			
			Thread.sleep(5000);
			String title = driver.getTitle();
			System.out.println("Title of the Main page is : "+title);
			
			driver.switchTo().frame(0);
			WebElement main = driver.findElement(By.cssSelector("div.content"));
			System.out.println("Main Frame is : "+main.getText());
			
			driver.switchTo().frame(0);
			WebElement sub1 = driver.findElement(By.cssSelector("div.content"));
			System.out.println("Sub Frame 1 is : "+sub1.getText());
			
			driver.switchTo().parentFrame();
			driver.switchTo().frame(1);
			WebElement sub2 = driver.findElement(By.cssSelector("div.content"));
			System.out.println("Sub Frame 2 is : "+sub2.getText());
		}
}