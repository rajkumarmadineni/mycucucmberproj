package seleniumProgs;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.firefox.FirefoxDriver;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.WebDriverWait;

	public class ExplicitWait {

	    public static void main(String[] args) {
	        // Create a new instance of the Firefox driver
	        WebDriver driver = new FirefoxDriver();
	                
	        //Open browser
	        driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");
	                
	        //Print title of page and heading on page
	        System.out.println("Page title is: " + driver.getTitle());
	        
	        //Click the start button
	        driver.findElement(By.cssSelector("#start > button")).click();
	        
	        //Find the hidden element
	        WebElement hiddenText = driver.findElement(By.cssSelector("#finish > h4"));
	        
	        //Wait till it becomes visible
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.visibilityOf(hiddenText));
	        
	        //Print the hidden text after it is visible
	        System.out.println("Hidden Text is: " + hiddenText.getText());
	        
	        //Close the Browser
	        driver.close();
	    }
}
