package seleniumProgs;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Screenshot {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.google.com");
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.titleIs("Google"));
		
		driver.findElement(By.name("q")).sendKeys("Rajkumar",Keys.RETURN);
		
		wait.until(ExpectedConditions.titleContains("Rajkumar"));
		
		String title = driver.getTitle();
		System.out.println("Title of the page is : "+title);
		Thread.sleep(5000);
		
		String filename = "Screenshot_"+System.currentTimeMillis();
		
        File screenshot = ((RemoteWebDriver) driver).getScreenshotAs(OutputType.FILE);
    	
        File outputFile = new File("screenshots/" + filename + ".jpeg");
	
        System.out.println(outputFile.getAbsolutePath());
	
        try {
            FileUtils.copyFile(screenshot, outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }	
	}

}
