package seleniumProgs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ReadAndInsertonPage {

	public static void main(String[] args) throws IOException, InterruptedException {
        /*
         * Variable Declaration
         */
	
        WebDriver driver = new FirefoxDriver();
        java.util.List<String> Tasks = new ArrayList<String>();
	
        /*
         * Excel File Handling
         */
	
        // To locate the path of excel file.
        FileInputStream file = new FileInputStream(new File("D:\\Eclipse\\Eclipse Programs\\credentials.xlsx"));
	
        // Initialize the excel file as a workbook.
        XSSFWorkbook workbook = new XSSFWorkbook(file);
	
        // initialize the Excel Sheet of the workbook, (0) denotes first sheet of the
        // workbook.
        XSSFSheet sheet = workbook.getSheetAt(0);
	
        // Get RowCount
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
	
        // Iterate through the rows and add all the tasks to the Task list
        for (int i = 0; i < rowCount+1; i++) {
            Tasks.add(sheet.getRow(i).getCell(0).getStringCellValue());
        }
	
        // close the excel file.
        file.close();
	
        /*
         * Using the Data From the Excel Sheet in Selenium
         */
	
        // Navigate to the activity page
        driver.get("https://www.training-support.net/selenium/todo-list");
	
        // Store references to important elements in variables
        WebElement taskInput = driver.findElement(By.id("taskInput"));
        WebElement taskButton = driver.findElement(By.cssSelector("button.ui:nth-child(2)"));
	
        // Iterate through the Tasks list and add every task to the list.
        for (String Task:Tasks) {
            taskInput.sendKeys(Task);
            taskButton.click();
            Thread.sleep(5000);
        }
	
        // Close the driver upon completion
        //driver.close();
	
	}

}
