package seleniumProgs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ReadfromWebAndnInsertintoExcel {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
        int row_num = 0;
        int col_num = 0;
	
        WebDriver driver = new FirefoxDriver();
        driver.get("https://training-support.net/selenium/tables");
	
        WebElement table = driver.findElement(By.id("sortableTable"));
        List<WebElement> rows = table.findElements(By.xpath("id('sortableTable')/tbody/tr"));
        ArrayList<ArrayList<String>> tableElements = new ArrayList<ArrayList<String>>(); 
	
        for (WebElement row:rows) {
            tableElements.add(new ArrayList());
            List<WebElement> cells = row.findElements(By.xpath("td"));
            for(WebElement cell:cells) {
                tableElements.get(row_num).add(cell.getText());
            }
            row_num++;
        }
	
        //FileInputStream input = new FileInputStream(new File("D:\\Eclipse\\Eclipse Programs\\credentials.xlsx"));
        FileOutputStream output = new FileOutputStream(new File("D:\\Eclipse\\Eclipse Programs\\New.xlsx"));
    
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet newsheet = workbook.createSheet();
        XSSFSheet sheet = workbook.getSheetAt(0);
	
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        Row row = sheet.getRow(0);
        int RowCount = tableElements.size();
	
        for (int i = 0; i < RowCount; i++) {
            int cellCount = tableElements.get(i).size();
            Row newRow = sheet.createRow(i);
            for (int j = 0; j < cellCount; j++) {
                Cell cell = newRow.createCell(j);
                cell.setCellValue(tableElements.get(i).get(j));
            }
        }	

        workbook.write(output);
        output.close();
        output.close();
    }

}
