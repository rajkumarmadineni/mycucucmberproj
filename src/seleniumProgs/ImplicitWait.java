package seleniumProgs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ImplicitWait {
	public static void main(String[] args) {
        // Create a new instance of the Firefox driver
		//System.setProperty("webdriver.gecko.driver", "D:\\Documents\\Selenium Documents\\geckodriver-v0.26.0-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Open browser
        driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");
                
        //Print title of page and heading on page
        System.out.println("Page title is: " + driver.getTitle());
        
        //Click the start button
        driver.findElement(By.cssSelector("#start > button")).click();
        
        //Search for an element that does not exist on the page
        WebElement hiddenText = driver.findElement(By.cssSelector("h4"));
        
        //Print the hidden text after it is visible
        System.out.println("Hidden Text is: " + hiddenText);
        
        //Close the Browser
        driver.close();
    }
}
