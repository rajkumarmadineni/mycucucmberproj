package Test;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
 
@CucumberOptions(features="src\\seleniumProgs", plugin  = {"pretty", "html:target/Destination", "json:target/cucumber.json"}, monochrome = true)
public class TestRunner extends AbstractTestNGCucumberTests {
}