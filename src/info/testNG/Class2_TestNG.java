package info.testNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Class2_TestNG {
    WebDriver driver;

    @Test	
    public void exampleTestCase() {
		// Check the title of the page
		String title = driver.getTitle();	
		
		//Print the title of the page
		System.out.println("Page title is: " + title);
	        //Assertion for page title
		Assert.assertEquals("Training Support", title);
		//Find the clickable link on the page and click it
		driver.findElement(By.tagName("a")).click();
	//Print title of new page
	System.out.println("New page title is: " + driver.getTitle());
    }
	
    @BeforeMethod
    public void beforeMethod() {
		//Create a new instance of the Firefox driver
		driver = new FirefoxDriver();
				
		//Put a Implicit wait, this means that any search for elements on the page could take the time the implicit wait is set for before throwing exception
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
		//And now use this to visit www,example.com
		driver.get("https://www.training-support.net");
    }
	
    @AfterMethod
    public void afterMethod() {
		//Close the browser
		driver.quit();
    }
}
