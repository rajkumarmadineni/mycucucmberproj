package info.testNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Class5_TestNG {
    WebDriver driver;
    WebDriverWait wait;
	
    @DataProvider(name = "Authentication")
    public static Object[][] credentials() {
        return new Object[][] { { "admin", "password" }, { "tom", "SecretPassword" } };
    }
	
    @BeforeMethod
    public void beforeMethod() {
        // Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
	
        // Create a new instance of WebDriverWait	
        wait = new WebDriverWait(driver, 10);

        //Open browser
        driver.get("https://www.training-support.net/selenium/login-form");
    }
	
    @Test(dataProvider = "Authentication")
    public void loginSuccessTestCase(String username, String password) {
        Reporter.log("Test case started | ");
	
        //Print title of Login Page
        Reporter.log("Saved Login Page heading as " + driver.getTitle()); 
	
        //Print the heading on the Login Page
        String loginHeading = driver.findElement(By.tagName("h2")).getText();;
        Reporter.log("Saved Login page heading as " + loginHeading);
	
        //Find the username field
        driver.findElement(By.id("username")).sendKeys(username);
        Reporter.log("Entered username");
	
        //Find the password field
        driver.findElement(By.id("password")).sendKeys(password);
        Reporter.log("Entered password");
	
        //Submit
        driver.findElement(By.cssSelector("button.ui:nth-child(4)")).click();
        Reporter.log("Clicked Login button");
	
        //Verify that the success message is visible
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("action-confirmation")));
        String confirmationHeading = driver.findElement(By.id("action-confirmation")).getText();
        Assert.assertEquals(confirmationHeading, "Welcome Back, admin");
        Reporter.log("Test case ended");
    }
	
    @AfterMethod
    public void afterMethod() {
        //Close the browser
        driver.close();
        Reporter.log("Browser closed");
    }
}