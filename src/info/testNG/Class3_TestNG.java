package info.testNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Class3_TestNG {
    WebDriver driver;	
    WebDriverWait wait;

    @Test
    public void loginSuccessTestCase() {

        //Print title of Login Page
        System.out.println("Saved Login Page heading as " + driver.getTitle());
		
        //Print the heading on the Login Page
        WebElement loginHeadingElement = driver.findElement(By.cssSelector("div.icon > div:nth-child(2)"));
        String loginHeading = loginHeadingElement.getText();
        System.out.println("Saved Login page heading as " + loginHeading);
	
        //Find the username field
        driver.findElement(By.id("username")).sendKeys("admin");
        System.out.println("Entered username");
	
        //Find the password field
        driver.findElement(By.id("password")).sendKeys("password");
        System.out.println("Entered password");
	
        //Submit
        driver.findElement(By.cssSelector("button.ui:nth-child(4)")).click();
        System.out.println("Clicked Login button");
	
        //Verify that the success message is visible
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("action-confirmation")));
        String confirmationHeading = driver.findElement(By.id("action-confirmation")).getText();
        Assert.assertEquals(confirmationHeading, "Welcome Back, admin");
        System.out.println("Test case ended");
    }
	
    @Test
    public void loginFailureTestCase() {
        //Print title of Login Page
        System.out.println("Saved Login Page heading as " + driver.getTitle());	
        //Print the heading on the Login Page
        WebElement loginHeadingElement = driver.findElement(By.cssSelector("div.icon > div:nth-child(2)"));
        String loginHeading = loginHeadingElement.getText();
        System.out.println("Saved Login page heading as " + loginHeading);
	
        //Find the username field
        driver.findElement(By.id("username")).sendKeys("tomsmith");
        System.out.println("Entered username");
 
        //Find the password field
        driver.findElement(By.id("password")).sendKeys("wrongPassword");
        System.out.println("Entered password");
	
        //Submit
        driver.findElement(By.cssSelector("button.ui:nth-child(4)")).click();	
        System.out.println("Clicked Login button");
	
        //Verify that the success message is visible
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("action-confirmation")));
        String confirmationHeading = driver.findElement(By.id("action-confirmation")).getText();
        Assert.assertEquals(confirmationHeading, "Welcome Back, admin");
        System.out.println("Test case ended");
    }
	
    @BeforeMethod
    public void beforeMethod() {
        // Create a new instance of the Firefox driver
        driver = new FirefoxDriver();	
        // Create a new instance of wait
        wait = new WebDriverWait(driver, 10);
        //Open browser
        driver.get("https://training-support.net/selenium/login-form");
    }
	
    @AfterMethod
    public void afterMethod() {
        //Close the browser
        driver.close();
        System.out.println("Browser closed");
    }
}