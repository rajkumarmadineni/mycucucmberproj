package info.testNG;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Class1_TestNG {
	
	WebDriver driver = null;
	@Test
	public void testing1() {
		String title = driver.getTitle();
		System.out.println("Current page Title is : "+title);
	}
	
	@BeforeTest
	public void before() {
		//creating driver instance and launching the URL
		driver = new FirefoxDriver();
		driver.get("https://www.training-support.net");
		
	}
	
	@AfterTest
	public void after() {
		//Closing the driver
		driver.close();
	}
	
	@Test
	public void testing2() {
		driver.findElement(By.name("searchBox"));
	}
	
	@Test (enabled = false)
	public void testing3() {
		//This test is not executed , we are ignoring and this will not be visible in report.
	}
	
	@Test
	public void testing4() {
		//This test will be skipped and will be be shown as skipped
    	throw new SkipException("Skipping test case");
	}
}