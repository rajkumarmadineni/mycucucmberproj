package info.testNG;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Class6_TestNG {
    WebDriver driver;
    WebDriverWait wait;
    Alert alert;
	
    @Test(priority = 0)
    public void simpleAlertTestCase() {
	
        Reporter.log("simpleAlertTestCase() started");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("simple")));
        //Click the button to open a simple alert
        driver.findElement(By.id("simple")).click();
        Reporter.log("Simple Alert opened");
	
        //Switch to alert window
        Alert simpleAlert = driver.switchTo().alert();
        Reporter.log("Switched foucs to alert");
	
        //Get text in the alert box and print it
        String alertText = simpleAlert.getText();
        Reporter.log("Alert text is: " + alertText);
        simpleAlert.accept();
        Reporter.log("Alert closed");
        Reporter.log("Test case ended");
    }
	
    @Test(priority = 1)
    public void confirmAlertTestCase() {
        Reporter.log("confirmAlertTestCase() started");
	
        //Click the button to open a simple alert
        driver.findElement(By.id("confirm")).click();
        Reporter.log("Confirm Alert opened");
	
        //Switch to alert window
        Alert confirmAlert = driver.switchTo().alert();
        Reporter.log("Switched foucs to alert");
	
        //Get text in the alert box and print it
        String alertText = confirmAlert.getText();
        Reporter.log("Alert text is: " + alertText);
	
        confirmAlert.accept();
        Reporter.log("Alert closed");
        Reporter.log("Test case ended");
    } 
	
    @Test(priority = 2)
    public void promptAlertTestCase() {
        Reporter.log("promptAlertTestCase() started");
	
        //Click the button to open a simple alert
        driver.findElement(By.id("prompt")).click();
        Reporter.log("Prompt Alert opened"); 
	
        //Switch to alert window
        Alert promptAlert = driver.switchTo().alert();
        Reporter.log("Switched foucs to alert");
	
        //Get text in the alert box and print it
        String alertText = promptAlert.getText();
        Reporter.log("Alert text is: " + alertText);
        promptAlert.sendKeys("Awesome!");
        Reporter.log("Text entered in prompt alert");
        promptAlert.accept();
        Reporter.log("Alert closed");
        Reporter.log("Test case ended");
    }
	
    @BeforeMethod
    public void beforeMethod() {
        Reporter.log("Test Case Setup started");
        driver.switchTo().defaultContent();
    }
	
    @BeforeTest
    public void beforeTest() {
        //Create a new instance of the Firefox driver
        driver = new FirefoxDriver();
	
        // Wait
        wait = new WebDriverWait(driver, 10);
        Reporter.log("Starting Test");
	
        //Open browser
        driver.get("https://www.training-support.net/selenium/javascript-alerts");
        Reporter.log("Opened Browser");
	
        //Print title of page
        Reporter.log("Page title is " + driver.getTitle());
    }
	
    @AfterTest
    public void afterTest() {
        Reporter.log("Ending Test");
        //Close the driver
        driver.close();
    }
}
