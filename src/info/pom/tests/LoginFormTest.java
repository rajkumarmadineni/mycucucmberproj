package info.pom.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import info.pom.objects.POMObjects;

public class LoginFormTest extends POMObjects {
   	
    @Test
    public void loginSuccessTestCase() {
        //Print title of Login Page
        System.out.println("Saved Login Page heading as " + getTitleOfThePage());
	
        //Print the heading on the Login Page
        String loginHeading = getTextOfTheElement(heading);
        System.out.println("Saved Login page heading as " + loginHeading);
	
        //login
        loginForm("admin", "password");
        System.out.println("Login successful..");
	
        //Verify that the success message is visible
        waitForAnElement(successMessage);
        String confirmationHeading = getTextOfTheElement(successMessage);
        Assert.assertEquals(confirmationHeading, "Welcome Back, admin");
        System.out.println("Test case ended");
    }
	
    @Test
    public void loginFailureTestCase() {
        //Print title of Login Page
        System.out.println("Saved Login Page heading as " + getTitleOfThePage());
	
        //Print the heading on the Login Page
        String loginHeading = getTextOfTheElement(heading);
        System.out.println("Saved Login page heading as " + loginHeading);
	
        //login
        loginForm("tomsmith", "wrongPassword");
        System.out.println("Login un successful..");
	
        //Verify that the success message is visible
        waitForAnElement(successMessage);
        String confirmationHeading = getTextOfTheElement(successMessage);
        Assert.assertEquals(confirmationHeading, "Welcome Back, admin");
        System.out.println("Test case ended");
    }
	
    @BeforeMethod
    public void beforeMethod() {
        // Create a new instance of the Firefox driver
        createDriver("firefox");
	
        //Open browser
        launchLRL(URL);
    }
	
    @AfterMethod
    public void afterMethod() {
        //Close the browser
        closeDriver();
        System.out.println("Browser closed");
    }
}