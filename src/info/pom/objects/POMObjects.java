package info.pom.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class POMObjects {
	
	WebDriver driver = null;
	WebDriverWait wait = null;
	
	public String URL = "https://training-support.net/selenium/login-form";
	public By heading = By.cssSelector("div.icon > div:nth-child(2)");
	public By userName = By.id("username");
	public By password = By.id("password");
	public By submit = By.cssSelector("button.ui:nth-child(4)");
	public By successMessage = By.id("action-confirmation");
	
	//Locating the element
	public WebElement locateElement(By element) {
		return driver.findElement(element);
	}
	
	//Creating the browser object
	public WebDriver createDriver(String driverName) {
		switch (driverName) {
		case "firefox":
						driver = new FirefoxDriver();
						break;
		case "ie":
						driver = new InternetExplorerDriver();
						break;
		case "edge":	
						driver = new EdgeDriver();
						break;
		case "chrome":
						driver = new ChromeDriver();
						break;
		default:
						System.out.println("Invalid driver entered...");
						break;
		}
		wait = new WebDriverWait(driver, 5);
		return driver;
	}
	
	//Launching the URL
	public void launchLRL(String url) {
		driver.get(url);
	}
	
	//Closing the browser
	public void closeDriver() {
		if (driver != null) {
			driver.close();
		}
	}
	
	//Setting text into the desired element
	public void setTextIntoTheElement(By elementPath, String text) {
    	locateElement(elementPath).sendKeys(text);
    }
	
	//Getting text of the desired element
	public String getTextOfTheElement(By elementPath) {
    	return locateElement(elementPath).getText();
    }
	
	//Gets title of the page
	public String getTitleOfThePage() {
		return driver.getTitle();
	}
	
	//Wait for a specific element
	public void waitForAnElement(By element) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
	//Clicking on the desired element
	public void clickOnElement(By element) {
		locateElement(element).click();
	}
	
	//Login form
	public void loginForm(String uname, String pwd ) {
		setTextIntoTheElement(userName, uname);
		setTextIntoTheElement(password, pwd);
		clickOnElement(submit);
	}
}