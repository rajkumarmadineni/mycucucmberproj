package javaActivity2;

public class Activity2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String array1[] = {"hi", "hello", "how are you"};
		int array2[] = {1,2,3,4,5,7,8};
		printStringArray(array1);
		printIntArray(array2);
	}
	
	static void printStringArray(String arr[]) {
		System.out.println("String array values are: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		System.out.println("---------------------");
	}
	
	static void printIntArray(int[] arr) {
		System.out.println("int array values are :");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		System.out.println("---------------------");
	}
}
